# Networks

This repository includes documentation on networks and communications sessions from Fablab BCN, in the fabacademy program.

Two folders:

- **local**: Using RF antennas to send sensor data from node to node
- **remote**: Using an MQTT broker in a remote server to receive sensor data. Each node is either a computer sending data over MQTT using a custom client implementation (for instance a python script as the example) or a different one. Also for those having WiFi compatible chips (ESP8266, ESP32, ...) there are examples for the nodes in the folder
