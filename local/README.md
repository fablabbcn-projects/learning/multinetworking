# Networking workshop

This is a compilation of the code from the Workshop: _hands-on communication_.

## Schema

![](img/connect.jpg)

## Items needed

1. Raspberry Pi
2. I2C sensor (in this case SHT31)
3. Two H12C RF nodes
4. (1 + nNodes) NRF24 boards
5. (1 + nNodes) arduino unos
6. Jumper cables
7. Breadboard

## Example Codes-Tutorials

- [Samples of networking-Class](https://hackmd.io/s/B15x5dn9V)

- [Max payload size(32bytes)](https://medium.com/@benjamindavidfraser/arduino-nrf24l01-communications-947e1acb33fb)

- [Multinetwork communication](https://howtomechatronics.com/tutorials/arduino/how-to-build-an-arduino-wireless-network-with-multiple-nrf24l01-modules/)

-[Multireceiver Nodes](https://www.instructables.com/id/NRF24L01-Multiceiver-Network/)

## Images of First Nodes

![](img/node1.jpg)
